$(document).ready(function(){

	var chatResult = $('#dialog');
    chatResult.scrollTop(chatResult.prop('scrollHeight'));
    /*----*/
	$('#add_cat').click(function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$('#cat').fadeOut(100);
		} else {
			$(this).addClass('active');
			$('#cat').fadeIn(100);
		}
	})
    

	/*---Maska-*/
	$("input[name='finishTime']").mask("99:99");	
	
	/*-vibor goroda-*/
	$('span.namecity').click(function(){
		$('.select_city').fadeOut(300);
		$(this).next('.select_city').fadeIn(300)
	})
	$('.select_city .krestik').click(function(){
		$('.select_city').fadeOut(300)
	});
	$('.select_city li').click(function() {
		$('.select_city').fadeOut(0)
		var city = $(this).text();
		$('.namecity').text(city);
	});
	/*--*/
	
	/*-menu_account-*/
	$('.account_in_site .name_login').click(function(){
		$('.menu_account').fadeIn(300);
	})
	$('.menu_account .krestik').click(function(){
		$('.menu_account').fadeOut(300)
	});
	
	/*--*/

	/*-ne standartniy selector-*/
	var sel=0;
	$('ul.selecter').click(function(){
		$('ul.selecter').removeClass('activ');	
		$('ul.selecter li').removeClass('activ');
		$('.selecter .close').slideUp(150);
		if(sel==1){
			sel=0;
		}else{
			$(this).addClass('activ');
			$(this).find('li.activ_punct').addClass('activ');
			$(this).find('.close').slideDown(150);
			sel=1;
			
		}
	});
	$('ul.selecter li:not(.activ_punct)').click(function(){
		var usl = $(this).find('span').html();
		var par = $(this).parent().parent();
		$(par).find('li.activ_punct span').html(usl);
		$(par).find('input').val(usl);
		$('ul.selecter').find('.close:not(.activ_punct)').slideUp(150);
		$('ul.selecter').removeClass('activ');	
		$('ul.selecter li').removeClass('activ');	
		$('ul.selecter').find('li.activ_punct').removeClass('activ');
		sel=1;
	});
	
	$(document).mouseup(function (e){ 
		var div = $('ul.selecter'); 
		if (!div.is(e.target)&& div.has(e.target).length === 0) { 
			$('ul.selecter').find('.close').slideUp(150);
			$('ul.selecter').removeClass('activ');	
			$('ul.selecter li').removeClass('activ');
			sel=0;				
		}
	});
	
	/*-Categorii-*/
	
	$('.category').hover(function(){
		$(this).find('.close').slideDown(150);
		$(this).find('.all').slideUp(0);
		$(this).css("z-index", "5");
	}, function() {
		$(this).find('.close').slideUp(150);
		$(this).find('.all').delay(200).slideDown(100);
		$(this).css("z-index", "0");
	})
	
	/*-Top-menu shadow-*/
	$(window).scroll(function() {
		if ($(this).scrollTop() < 1) {
			$('header').removeClass('activ');
		}
		if ($(this).scrollTop() > 0) {
			$('header').addClass('activ');
		}
	});
	
	/*-Video open-*/
	$('#glow').click(function(){
		$('body').append('<div id="fade"></div>'); 
		$('#fade').fadeIn(100);
		$(this).next().next('#open_video').fadeIn(900).prepend('<div class="close"></div>');	
		vid = $(this).next().next('#open_video').find("iframe").attr("src");
		var video =  $(this).next().next('#open_video').find("iframe").attr("src");
         $(this).next().next('#open_video').find("iframe").attr("src",video+"?autoplay=1");
	})
	
	$('#open_video .close, #fade').live('click', function() {
		$("#open_video iframe").attr("src","");
        $("#open_video iframe").attr("src",vid);
			
	  	$('#fade').fadeOut(600); 
	  	$('#open_video').fadeOut(100); 
		$('#open_video .close').remove();  
	});
	
	/*-HELP Block-*/
	$('.lable_block').focusin(function(){
		$(this).find('.help').addClass('activ');
	});
	$('.lable_block').focusout(function(){
		$(this).find('.help').removeClass('activ');
	});
	/*--*/
	
	/*-input sale-*/
	$('.input_activ').focusin(function(){
		$(this).find('input#my_sale').prop('checked', true)
	});
	
	/*--Vibor DATE--*/
	$('.day').click(function(){	
		$('.day').removeClass('activ');
		$(this).addClass('activ');
		var date = $(this).find('span').html();
		$('input#finish').val(date);
	})
	
	$('.calendar').click(function(){	
		$('.day').removeClass('activ');
	})
	
	/*--input Map--*/
	$('#list_map li').focusin(function(){
		$(this).find('span').addClass('activ');
	});
	$('.lable_block').focusout(function(){
		$(this).find('span').removeClass('activ');
	});
	/**/
	
	/*--*/	
	$('.btn_private_info').click(function(){
		$('#privat_block').slideDown(150);
		$('.btn_private_info').fadeOut(300);
	})
	$('#privat_block .close').click(function(){
		$('#privat_block').slideUp(150);
		$('.btn_private_info').fadeIn(300);
	})


	
})


