$(document).ready(function() {
	
	if ($('#list_tasks').length > 0) {
		var obj = $('#list_tasks > div:not([class])');
		obj.each(function() {
			var h = $(this).find('.cap').height();
			if (h > 40) {
				$(this).find('.fio').css({
					'margin-top': '5px'
				});
			}
		});
	}
	
	$('#all_task .change_city a').click(function() {
		var h = $('#select_city').height()/2;
		$('#city_shadow').fadeIn(300);
		$('#select_city').css({
			'margin-top': '-' + h + 'px'
		}).delay(200).fadeIn(300);
		return false;
	});
	
	$('#select_city .exit, #city_shadow').click(function() {
		$('#city_shadow').delay(200).fadeOut(300);
		$('#select_city').fadeOut(300);
	});
	
	$('#filter_map .f-map > div, #filter_map .f-map a, .f-map').click(function() {
		if (!$('#cont-map').is(':visible')) {
			$('#cont-map').slideDown(400);
		}
		return false;
	});
	
	$('#cont-map .hide').click(function() {
		$('#cont-map').slideUp(400);
	});
	
	$('#task_item .map-resize a').click(function() {
		if (!$(this).hasClass('show')) {
			$(this).addClass('show');
			$(this).text("Свернуть карту");
			$('#map-t').css({
				'height': '670px'
			});
			myMap.container.fitToViewport();
		} else {
			$(this).removeClass('show');
			$(this).text("Развернуть карту");
			$('#map-t').css({
				'height': '180px'
			});
			myMap.container.fitToViewport();
		}
		return false;
	});
	
	$('#task_item .task_share_link').click(function() {
		if (!$(this).hasClass('show')) {
			$(this).addClass('show');
			$('#task_item .ya-share2').slideDown(300);
		} else {
			$(this).removeClass('show');
			$('#task_item .ya-share2').slideUp(300);
		}
		return false;
	});
	
});