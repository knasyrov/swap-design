# Замечания по верстке #

### CSS селекторы классы и id ###

Именуется через дефис (общепринятое)
не `modal_hello_world`, а `modal-hello-world`

### Все контроллеры (`input`, `textarea`, `radio`, `checkbox`, `select`) обрамляем в `form-control` ###

неправильно:
``` 
#!slim
div.birka
  input name='data'
```
правильно:
```
#!slim
div.birka
  .form-control
    input name='data'
```
или
```
#!slim
.form-control
  div.birka
    input name='data'
```

### Селект (который `ul.selecter` ) ###

Правильная его структура:
```
#!slim
ul.el.selecter
  .arrow_selector
  li.option.active data-id="48" 48 часов
  .options
    li.option data-id="48" 48 часов
    li.option data-id="24" 24 часа
    li.option data-id="12" 12 часов
    li.option data-id="6" 6 часов
    li.option data-id="2" 2 часа
    li.option data-id="1" 1 час
```

если он у тебя изменен по размеру
то можно делать что-то вроде:
```
#!slim
ul.el.selecter.size-120
```
или обрамляем:

```
#!slim
.panel
  .form-control
    ul.el.selecter
```

### Диалоги ###
ВСЕ ДИАЛОГИ ОДИНАКОВЫЕ - без извращений
если надо что-то изменить добавляем `class` или `id`

```
#!slim
.modal-dialog#my-custom-dialog
  .modal-content
    .modal-header
      ВЕРХУШКА
      button.close aria-label="Close" data-dismiss="modal" type="button" 
        span aria-hidden="true"
      h4.modal-title ЗАГОЛОВОК
    .modal-body
      СОДЕРЖИМОЕ
    .modal-footer
      ПОДВАЛ
```

### `radio` и `checkbox` ###

Правильная структура

```
#!slim

.radio.form-control
  label
    input#male type='radio'
    Мужской
  label
    input#female type='radio'
    Женский

.checkbox.form-control
  label
    input#accept-rules type='checkbox'
    Я принимаю правила
```

**сделай такие классы - чтобы были универсальные!!!**

### Сетка ###

Должно быть что-то типа

```
#!slim

.row
  .col-120
    первая колонка
  .col-240
    вторая колонка
  .col-120
    третяя колонка

.row
  .col-240
    первая колонка
  .col-240
    вторая колонка

```

для примера
![Заработай.ру 2016-03-14 16-21-55.png](https://bitbucket.org/repo/dE8oae/images/4050172548-%D0%97%D0%B0%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0%D0%B8%CC%86.%D1%80%D1%83%202016-03-14%2016-21-55.png)

будет

```
#!slim

.row.gray
  .col-20
  .col-500
    .checkbox.form-control
      label
        input#notify name='notify' type='checkbox'
        Уведомить меня, если работником выберут другого, или если задание будет отменено.
.row
  .col-20
  .col-300
    .checkbox.form-control
      label
        input#time-limited name='time_limited' type='checkbox'
        Указать время актуальности предложения:
  .col-150.form-control
    ul.el.selecter
      .arrow_selector
      li.option.active data-id="48" 48 часов
        .options
          li.option data-id="48" 48 часов
          li.option data-id="24" 24 часа
          li.option data-id="12" 12 часов
          li.option data-id="6" 6 часов
          li.option data-id="2" 2 часа
          li.option data-id="1" 1 час
  .col-50
    .clock
```

**Сетка есть - но не везде что-то работает - поправь!!!**